(* Once you are done writing the code, remove this directive,
   whose purpose is to disable several warnings. *)
[@@@warning "-27-34-37-39"]

(****************************************************************)
(* Simple types, annotated by OCaml types                       *)
(****************************************************************)

(* *Deep* embedding of types:

<<<
    τ, σ ::= Ω         (* base type *)
           | τ → σ     (* arrrow type *)
>>>
 *)

(* Use a GADT to relate the syntactic [tp] with OCaml's types *)

type 'a tp = |

(* NYI *)
and base = Atom of base at

(* = Ω *)

(* In particular, we want to be ensure that an arrow in [tp] will
   correspond to an OCaml function during reflection. *)

(****************************************************************)
(* Typed source language                                        *)
(****************************************************************)

(* *Deep* embedding of terms using weak/parametric higher-order
   abstract syntax (HOAS):

<<<
    t, u ::= x
           | λ x. t
           | t u
>>>
 *)

(* Use a GADT to make sure that we can only write well-typed terms. *)
and 'a tm = Var : 'a var -> 'a tm

(* | NYI: add missing cases *)

(****************************************************************)
(* Intermediate language of values, typed                       *)
(****************************************************************)

(* *Shallow* embedding of values in weak-head normal form:

<<<
    v ::= λ x. v
        | base

>>>
 *)

(* Use a GADT to make sure that we can only write well-typed values. *)
and 'a vl =
  | VFun : {t = 'nyi. 'nyi ->'nyi vl}
  | VBase : base -> base vl

(****************************************************************)
(* Typed target language: β-normal, η-long λ-terms              *)
(****************************************************************)

(* Use a GADT to make sure that we can only write β-normal, η-long
   terms. *)
and 'a nf = |

(* NYI *)
and 'a at = AVar : 'a y -> 'a at

(* | NYI: add missing cases *)
and 'a y

and 'a var = 'a vl

(****************************************************************)
(* Examples                                                     *)
(****************************************************************)

(* Define [typ1] as [Ω → Ω] *)

let typ1 () = failwith "NYI"

(* Define [typ2] as [(Ω → Ω) → Ω → Ω] *)

let typ2 () = failwith "NYI"

(* Define [tm1] as [λ x. x] *)

let tm1 () = failwith "NYI"

(* Define [tm2] as [λ f. λ x. f x] *)

let tm2 () = failwith "NYI"

(* Define [tm3] as [λ x. (λ y. y) x] *)
let tm3 () = failwith "NYI"

(* Define [vl1] as [λ x. x] *)

let vl1 () = failwith "NYI"

(* Define [vl2] as [λ f. λ x. f x] *)

let vl2 () = failwith "NYI"

(* Define [vl3] as [λ x. (λ y. y) x] *)
let vl3 () = failwith "NYI"

(* Define [nf1] as [λ x. x] *)

let nf1 () = failwith "NYI"

(* Define [nf2] as [λ f. λ x. f x] *)

let nf2 () = failwith "NYI"

(* Define [nf3] as [λ x. (λ y. y) x] *)
let nf3 () = failwith "NYI"

(****************************************************************)
(* Evaluation function: from source to intermediate             *)
(****************************************************************)

let rec eval : type a. a tm -> a vl = fun _ -> failwith "NYI"

(****************************************************************)
(* reify and reflect: from intermediate to target               *)
(****************************************************************)

let rec reify : type a. a tp -> a vl -> a nf = fun a v -> failwith "NYI"

and reflect : type a. a tp -> a at -> a vl = fun a r -> failwith "NYI"

(****************************************************************)
(* Normalization: from term to normal form                      *)
(****************************************************************)

let nbe : type a. a tp -> a tm -> a nf = fun a m -> failwith "NYI"

let%test_unit _ =
  let typ1 = typ1 () in
  let tm1 = tm1 () in
  ignore (nbe typ1 tm1)

let%test_unit _ =
  let typ2 = typ2 () in
  let tm2 = tm2 () in
  ignore (nbe typ2 tm2)

let%test_unit _ =
  let typ2 = typ2 () in
  let tm1 = tm1 () in
  ignore (nbe typ2 tm1)

let%test_unit _ =
  let typ1 = typ1 () in
  let tm3 = tm3 () in
  ignore (nbe typ1 tm3)

let%test_unit _ =
  let typ2 = typ2 () in
  let tm3 = tm3 () in
  ignore (nbe typ2 tm3)

(****************************************************************)
(* Outro                                                        *)
(****************************************************************)

(* Puzzle: did you notice that I didn't try to write
   pretty-printers or equality tests, what am I hiding up my
   sleeve? *)

(* Puzzle: [untyped.ml] unties the self-reference through polymorphic variables.
   Can you do the same here? *)

(* Hungry for more?
   -> "Typeful Normalization by Evaluation", Danvy, Keller & Puech
      [https://hal.inria.fr/hal-01397929/document] *)
